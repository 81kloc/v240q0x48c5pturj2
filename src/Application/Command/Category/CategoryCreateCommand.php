<?php
declare(strict_types = 1);

namespace App\Application\Command\Category;

use App\Domain\Entity\Category;

class CategoryCreateCommand
{
    /**
     * @var Category
     */
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }
}