<?php
declare(strict_types=1);

namespace App\Application\Command\Category;

use App\Domain\Entity\User;

class CategoryFindByIdCommand
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var User
     */
    private $user;

    /**
     * CategoryFindByIdCommand constructor.
     * @param int $id
     * @param User $user
     */
    public function __construct(int $id, User $user)
    {
        $this->id = $id;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

}