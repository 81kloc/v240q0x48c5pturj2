<?php
declare(strict_types=1);

namespace App\Application\Command\Category;

use App\Domain\Entity\User;

class CategoryListByUserCommand
{
    /**
     * @var User
     */
    private $user;

    /**
     * CategoryListByUserCommand constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}