<?php
declare(strict_types=1);

namespace App\Application\Command\Ping;

use App\Domain\Entity\Ping;

class PingCreateCommand
{
    /**
     * @var Ping
     */
    private $ping;

    public function __construct(Ping $ping)
    {
        $this->ping = $ping;
    }

    /**
     * @return Ping
     */
    public function getPing(): Ping
    {
        return $this->ping;
    }
}