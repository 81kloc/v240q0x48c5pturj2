<?php
declare(strict_types=1);

namespace App\Application\Command\Ping;

class PingGetByHashCommand
{
    /**
     * @var string
     */
    private $hash;

    public function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }


}