<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Category;

use App\Application\Command\Category\CategoryCreateCommand;
use App\Application\CommandInterface\Category\CategoryCreateCommandHandlerInterface;
use App\Domain\Repository\CategoryRepository;

class CategoryCreateCommandHandler implements CategoryCreateCommandHandlerInterface
{
    /**
     * @var CategoryRepository
     */
    private $repository;

    /**
     * CategoryCreateCommandHandler constructor.
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CategoryCreateCommand $categoryCreateCommand
     * @return \App\Domain\Entity\Category
     */
    public function __invoke(CategoryCreateCommand $categoryCreateCommand)
    {
        return $this->repository->create($categoryCreateCommand->getCategory());
    }

}