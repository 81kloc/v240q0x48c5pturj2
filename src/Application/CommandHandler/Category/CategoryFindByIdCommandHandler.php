<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Category;

use App\Application\Command\Category\CategoryFindByIdCommand;
use App\Application\CommandInterface\Category\CategoryFindByIdCommandHandlerInterface;
use App\Domain\Repository\CategoryRepository;

class CategoryFindByIdCommandHandler implements CategoryFindByIdCommandHandlerInterface
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * CategoryFindByIdCommandHandler constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function __invoke(CategoryFindByIdCommand $categoryFindByIdCommand)
    {
        return $this->categoryRepository->findById($categoryFindByIdCommand->getId(), $categoryFindByIdCommand->getUser());
    }
}