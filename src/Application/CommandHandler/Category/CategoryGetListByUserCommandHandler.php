<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Category;

use App\Application\Command\Category\CategoryListByUserCommand;
use App\Application\CommandInterface\Category\CategoryGetListByUserCommandHandlerInterface;
use App\Domain\Repository\CategoryRepository;

class CategoryGetListByUserCommandHandler implements CategoryGetListByUserCommandHandlerInterface
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function __invoke(CategoryListByUserCommand $categoryGetListByUserCommand)
    {
        return $this->categoryRepository->getListByUser($categoryGetListByUserCommand->getUser());
    }

}