<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Category;

use App\Application\Command\Category\DeleteCategoryCommand;
use App\Application\CommandInterface\Category\DeleteCategoryCommandHandlerInterface;
use App\Domain\Repository\CategoryRepository;

class DeleteCategoryCommandHandler implements DeleteCategoryCommandHandlerInterface
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function __invoke(DeleteCategoryCommand $deleteCategoryCommand)
    {
        $this->categoryRepository->delete($deleteCategoryCommand->getCategory());
    }

}