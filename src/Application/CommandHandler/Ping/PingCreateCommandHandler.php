<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Ping;

use App\Application\Command\Ping\PingCreateCommand;
use App\Application\CommandInterface\Ping\PingCreateCommandHandlerInterface;
use App\Domain\Repository\PingRepository;

class PingCreateCommandHandler implements PingCreateCommandHandlerInterface
{
    /**
     * @var PingRepository
     */
    private $pingRepository;

    public function __construct(PingRepository $pingRepository)
    {
        $this->pingRepository = $pingRepository;
    }

    public function __invoke(PingCreateCommand $pingCreateCommand)
    {
        $this->pingRepository->create($pingCreateCommand->getPing());
    }

}