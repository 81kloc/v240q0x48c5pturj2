<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Ping;

use App\Application\Command\Ping\PingGetByHashCommand;
use App\Application\CommandInterface\Ping\PingGetByHashCommandHandlerInterface;
use App\Domain\Repository\PingRepository;

class PingGetByHashCommandHandler implements PingGetByHashCommandHandlerInterface
{
    /**
     * @var PingRepository
     */
    private $pingRepository;

    public function __construct(PingRepository $pingRepository)
    {
        $this->pingRepository = $pingRepository;
    }

    public function __invoke(PingGetByHashCommand $pingGetByHashCommand)
    {
        return $this->pingRepository->getByHash($pingGetByHashCommand->getHash());
    }
}