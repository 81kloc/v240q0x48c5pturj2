<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Ping;

use App\Application\Command\Ping\PingGetByHashCommand;
use App\Application\CommandInterface\Ping\PingGetListCommandHandlerInterface;
use App\Domain\Repository\PingRepository;


class PingGetListCommandHandler implements PingGetListCommandHandlerInterface
{
    /**
     * PingGetListCommandHandler constructor.
     * @param PingRepository $repository
     */
    public function __construct(PingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(PingGetByHashCommand $pingGetListCommand)
    {
        $this->repository->getList();
    }

}