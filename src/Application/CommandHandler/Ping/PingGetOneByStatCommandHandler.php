<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\Ping;

use App\Application\Command\Ping\PingGetOneByStatCommand;
use App\Application\CommandInterface\Ping\PingGetOneByStatCommandHandlerInterface;
use App\Domain\Repository\PingRepository;

class PingGetOneByStatCommandHandler implements PingGetOneByStatCommandHandlerInterface
{
    public function __construct(PingRepository $repository)
    {
        $this->repository = $repository;
    }
    public function __invoke(PingGetOneByStatCommand $pingGetListCommand)
    {
       return $this->repository->getOneByStat();
    }
}