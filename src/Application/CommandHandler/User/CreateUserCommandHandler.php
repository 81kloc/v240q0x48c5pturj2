<?php
declare(strict_types=1);

namespace App\Application\CommandHandler\User;

use App\Application\Command\User\CreateUserCommand;
use App\Application\CommandInterface\User\CreateUserCommandHandlerInterfaces;
use App\Domain\Repository\UserRepository;

class CreateUserCommandHandler implements CreateUserCommandHandlerInterfaces
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param CreateUserCommand $createUserCommand
     */
    public function __invoke(CreateUserCommand $createUserCommand)
    {

        $this->userRepository->create($createUserCommand->getUser());
    }
}