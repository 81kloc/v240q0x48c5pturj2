<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Category;

use App\Application\Command\Category\CategoryCreateCommand;

interface CategoryCreateCommandHandlerInterface
{
    public function __invoke(CategoryCreateCommand $categoryCreateCommand);
}