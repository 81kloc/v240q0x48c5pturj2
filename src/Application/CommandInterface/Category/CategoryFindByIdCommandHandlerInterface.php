<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Category;


use App\Application\Command\Category\CategoryFindByIdCommand;

interface CategoryFindByIdCommandHandlerInterface
{
    public function __invoke(CategoryFindByIdCommand $categoryFindByIdCommand);
}