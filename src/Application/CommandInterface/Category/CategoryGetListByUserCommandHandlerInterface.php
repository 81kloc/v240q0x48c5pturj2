<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Category;


use App\Application\Command\Category\CategoryListByUserCommand;

interface CategoryGetListByUserCommandHandlerInterface
{
    public function __invoke(CategoryListByUserCommand $categoryGetListByUserCommand);
}