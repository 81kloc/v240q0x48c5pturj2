<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Category;

use App\Application\Command\Category\DeleteCategoryCommand;

interface DeleteCategoryCommandHandlerInterface
{
    public function __invoke(DeleteCategoryCommand $deleteCategoryCommand);
}