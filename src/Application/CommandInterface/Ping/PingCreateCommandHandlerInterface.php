<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Ping;

use App\Application\Command\Ping\PingCreateCommand;

interface PingCreateCommandHandlerInterface
{
    public function __invoke(PingCreateCommand $pingCreateCommand);
}