<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Ping;

use App\Application\Command\Ping\PingGetByHashCommand;

interface PingGetByHashCommandHandlerInterface
{
    public function __invoke(PingGetByHashCommand $pingGetByHashCommand);
}