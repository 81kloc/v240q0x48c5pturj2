<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Ping;

use App\Application\Command\Ping\PingGetByHashCommand;

interface PingGetListCommandHandlerInterface
{
    public function __invoke(PingGetByHashCommand $pingGetListCommand);
}