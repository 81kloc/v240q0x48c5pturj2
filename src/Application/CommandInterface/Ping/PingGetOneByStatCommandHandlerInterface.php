<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\Ping;

use App\Application\Command\Ping\PingGetOneByStatCommand;

interface PingGetOneByStatCommandHandlerInterface
{
    public function __invoke(PingGetOneByStatCommand $pingGetListCommand);
}