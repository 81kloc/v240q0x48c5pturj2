<?php
declare(strict_types=1);

namespace App\Application\CommandInterface\User;

use App\Application\Command\User\CreateUserCommand;

interface CreateUserCommandHandlerInterfaces
{
    public function __invoke(CreateUserCommand $createUserCommand);
}