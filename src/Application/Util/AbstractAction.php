<?php
declare(strict_types=1);

namespace App\Application\Util;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Messenger\MessageBusInterface;

abstract class AbstractAction extends Controller
{
    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * Query for asked data.
     *
     * @param object $commandRequest
     *
     * @return mixed
     */
    public function ask($commandRequest)
    {
        return $this->messageBus->dispatch(
            $commandRequest
        );
    }
}