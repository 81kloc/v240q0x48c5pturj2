<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Application\Command\User\CreateUserCommand;
use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateUserFixtures extends Fixture
{
    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('global@global.net');
        $user->setUsername('Global');
        $user->setEnabled(1);
        $user->setEmailCanonical('global@global.net');
        $user->setPlainPassword('test123');

        return $this->messageBus->dispatch(
            new CreateUserCommand($user)
        );
    }

}