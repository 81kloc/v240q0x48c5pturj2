<?php
declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Category;
use App\Domain\Entity\User;

interface CategoryRepository
{
    public function create(Category $category) : void;

    public function getListByUser(User $user): ?array;

    public function delete(Category $category): void;

    public function findById(int $id, User $user): ?array;
}