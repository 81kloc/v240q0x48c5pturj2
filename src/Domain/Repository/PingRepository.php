<?php
declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\Ping;

interface PingRepository
{
    public function getByHash(string $hash): ?Ping;

    public function create(Ping $ping): void;

    public function update(Ping $ping): void;

    public function getOneByStat(): ?Ping;
}