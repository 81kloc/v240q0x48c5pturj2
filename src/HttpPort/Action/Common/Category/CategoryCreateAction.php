<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Common\Category;

use App\Application\Command\Category\CategoryCreateCommand;
use App\Application\Util\AbstractAction;
use App\Domain\Entity\Category;
use App\Infrastructure\Form\CategoryType;
use Symfony\Component\HttpFoundation\Request;

class CategoryCreateAction extends AbstractAction
{
    public function __invoke(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->$this->ask(new CategoryCreateCommand($this->createCategory($request)));

            return $this->redirectToRoute('category_index');
        }

        return $this->render('common/category/create.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    private function createCategory($request): Category
    {
        $category = new Category();
        $category->setName($request->get('name'));
        $category->setUser($this->getUser());

        return $category;
    }
}