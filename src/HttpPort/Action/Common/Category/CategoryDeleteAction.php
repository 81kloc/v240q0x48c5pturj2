<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Common\Category;

use App\Application\Command\Category\CategoryFindByIdCommand;
use App\Application\Command\Category\DeleteCategoryCommand;
use App\Application\Util\AbstractAction;

class CategoryDeleteAction extends AbstractAction
{
    public function __invoke(int $id)
    {
        $categoryObj = $this->ask(new CategoryFindByIdCommand($id, $this->getUser()));

        if($categoryObj){

            $this->ask(new DeleteCategoryCommand($categoryObj));
        }

        return $this->redirectToRoute('category_index');
    }
}