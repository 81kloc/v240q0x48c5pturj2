<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Common\Category;

use App\Application\Command\Category\CategoryListByUserCommand;
use App\Application\Util\AbstractAction;

class CategoryListByUserAction extends AbstractAction
{
    public function __invoke()
    {
        $categories = $this->ask(new CategoryListByUserCommand($this->getUser()));

        return $this->render('common/category/list.html.twig', [
            'categories' => $categories,
        ]);
    }
}