<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Common\Home;

use App\Domain\Entity\Ping;
use App\Infrastructure\Form\PingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexAction extends Controller
{
    public function __invoke()
    {
        $ping = new Ping();
        $form = $this->createForm(PingType::class, $ping);

        return $this->render('common/home/index.html.twig', ['form' => $form->createView(),]);
    }
}