<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Common\Ping;

use App\Application\Command\Ping\PingCreateCommand;
use App\Application\Util\AbstractAction;
use App\Domain\Entity\Ping;
use App\Infrastructure\Form\PingType;
use Symfony\Component\HttpFoundation\Request;

class PingCreateAction extends AbstractAction
{
    public function __invoke(Request $request)
    {
        $ping = new Ping();
        $form = $this->createForm(PingType::class, $ping);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->ask(new PingCreateCommand($this->createPing($request)));

            return $this->redirectToRoute('ping_hash');
        }

        return $this->render('common/ping/create.html.twig', [
            'ping' => $ping,
            'form' => $form->createView(),
        ]);
    }

    private function createPing(Request $request): Ping
    {
        $ping = new Ping();
        $ping->setUrl($request->get('url'));
        $ping->setTitle($request->get('title'));
        $ping->setHash(md5($request->get('url')));
        $ping->setCreatedAt(new \DateTime("now"));

        return $ping;
    }
}