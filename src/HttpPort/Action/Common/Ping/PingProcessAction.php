<?php
declare(strict_types=1);

namespace App\HttpPort\Action\Common\Ping;

use App\Application\Command\Ping\PingGetByHashCommand;
use App\Application\Util\AbstractAction;

class PingProcessAction extends AbstractAction
{
    public function __invoke(string $hash)
    {
        $ping = $this->ask(new PingGetByHashCommand($hash));

        return $this->render('common/ping/process.html.twig', [
            'ping' => $ping,
        ]);
    }
}