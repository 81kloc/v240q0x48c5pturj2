<?php
declare(strict_types=1);

namespace App\Infrastructure\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PingCommand extends Command
{
    protected static $defaultName = 'app:ping';

    protected function configure()
    {
        $this->setDescription('Ping url.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo 'ping';
    }
}