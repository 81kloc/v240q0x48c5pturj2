<?php
declare(strict_types=1);

namespace App\Infrastructure\Form;

use App\Domain\Entity\Ping;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class PingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', TextType::class, [
                'label' => 'Url',
                'required' => true,
                'constraints' => [new Length(['max' => 255]), new NotBlank()],
                'attr' => [
                    'placeholder' => 'http://www.domain.com',
                    'maxlength' => 255,
                ],
            ])
            ->add('title', TextType::class, [
                'label' => 'Title',
                'required' => true,
                'constraints' => [new Length(['max' => 255]), new NotBlank()],
                'attr' => [
                    'placeholder' => 'Title',
                    'maxlength' => 255,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ping::class,
        ]);
    }
}