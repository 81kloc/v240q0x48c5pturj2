<?php
declare(strict_types = 1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Category;
use App\Domain\Entity\User;
use App\Domain\Repository\CategoryRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineCategoryRepository extends EntityRepository implements CategoryRepository
{
    public function create(Category $category): void
    {
        $this->_em->persist($category);
        $this->_em->flush();
    }

    public function getListByUser(User $user): ?array
    {
        return $this->findBy(['user' => $user]);
    }

    public function delete(Category $category): void
    {
        $this->_em->remove($category);
        $this->_em->flush();
    }

    public function findById(int $id, User $user): ?array
    {
        return $this->findOneBy(['user' => $user, 'id' => $id]);
    }
}