<?php
declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Ping;
use App\Domain\Repository\PingRepository;
use Doctrine\ORM\EntityRepository;

class DoctrinePingRepository extends EntityRepository implements PingRepository
{
    public function getByHash(string $hash): ?Ping
    {
        return $this->findOneBy(['hash' => $hash]);
    }

    public function create(Ping $ping): void
    {
        $this->_em->persist($ping);
        $this->_em->flush();
    }

    public function update(Ping $ping): void
    {
        $this->_em->persist($ping);
        $this->_em->flush();
    }

    public function getOneByStat(): ?Ping
    {
        return $this->findOneBy(['pingStat' => 0]);
    }
}