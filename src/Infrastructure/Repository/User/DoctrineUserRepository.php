<?php
declare(strict_types=1);

namespace App\Infrastructure\Repository\User;

use App\Domain\Entity\User;
use App\Domain\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineUserRepository extends EntityRepository implements UserRepository
{
    public function create(User $user): void
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }
}